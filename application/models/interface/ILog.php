<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Log model interface
 * @author Escobeitor
 *
 */
interface ILog {
	
	/**
	 * Retrieves the log data associated to the
	 * given log identification number
	 * @param $gameId
	 */
	public function retrieveLog($gameId);
	
	/**
	 * Saves the given log data
	 * @param $log
	 */
	public function save($log);
}