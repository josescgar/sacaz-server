<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User model interface
 * @author Escobeitor
 *
 */
interface IUser {
	
	/**
	 * Saves a new user or updates a user
	 * @param $email
	 * @param $authKey
	 * @param $registrationId
	 */
	public function save($email, $authKey, $registrationId);
	
	/**
	 * Increases the given user's game result counters
	 * @param $userId
	 * @param $amount
	 * @param $result
	 */
	public function manageResultCounter($userId, $amount, $result);
	
	/**
	 * Saves a username for the user identified by the given authKey
	 * @param $authKey
	 * @param $username
	 */
	public function saveUsername($authKey, $username);
}