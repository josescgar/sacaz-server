<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Notification model interface
 * @author Escobeitor
 *
 */
interface INotification {
	
	/**
	 * Saves a new notification into the database
	 * @param unknown $timeStamp
	 * @param unknown $recipient
	 * @param unknown $message
	 * @param unknown $game
	 * @param unknown $sender
	 */
	public function save($timeStamp, $recipient, $message, $game, $sender);
}