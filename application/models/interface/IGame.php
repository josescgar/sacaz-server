<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Game model interface
 * @author Escobeitor
 *
 */
interface IGame {
	
	/**
	 * Returns the game data associated to the given game
	 * @param $gameId
	 */
	public function retrieveGameData($gameId);
	
	/**
	 * Returns a list of unfinished games for the given player
	 * @param $playerId
	 */
	public function retrieveUnfinishedGames($playerId);
	
	/**
	 * Updates a game setting the given fields to the given values
	 * @param $gameId
	 * @param $fields
	 * @param $values
	 */
	public function updateGame($gameId, $fields, $values);
	
	/**
	 * Deletes a given game from the database
	 * @param $gameId
	 */
	public function delete($gameId);
	
	/**
	 * Saves the given game
	 * @param $gameData
	 */
	public function save($gameData);
}