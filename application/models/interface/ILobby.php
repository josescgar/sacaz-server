<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Lobby model interface
 * @author Escobeitor
 *
 */
interface ILobby {
	
	/**
	 * Checks if a lobby title is available
	 * @param $title
	 */
	public function isTitleAvailable($title);
	
	/**
	 * Creates a new lobby
	 * @param $title
	 * @param $level
	 * @param $faction
	 * @param $userId
	 */
	public function registerLobby($title, $level, $faction, $userId);
	
	/**
	 * Removes a lobby
	 * @param $id
	 */
	public function removeLobbyInfo($id);
	
	/**
	 * Joins a player into a lobby
	 * @param $title
	 * @param $userId
	 */
	public function addPlayerToLobby($title, $userId);
	
	/**
	 * Check all available games matching the criteria
	 * @param $level
	 * @param $faction
	 */
	public function checkAvailableGames($level, $faction);
	
	/**
	 * Get the list of available lobbies for the given user (paginated)
	 * @param $userId
	 * @param $page
	 */
	public function getUserLobbies($userId, $page);
}
