<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lobby model MongoDB implementation
 * @author Escobeitor
 *
 */
class MongoLobby extends CI_Model {
	
	/**
	 * Database instance
	 * @var
	 */
	public $mongo;
	
	/**
	 * Default constructor. Load MongoDB library.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->config->load('mongodb');
		$this->mongo = buildMongoConnection();
	}

	/**
	 * @see ILobby::isTitleAvailable()
	 */
	public function isTitleAvailable($title) {
		try {
			return $this->mongo->lobby->count(array('title' => $title), 1) == 0;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::registerLobby()
	 */
	public function registerLobby($title, $level, $faction, $userId) {
		try {
			
			$user = $this->mongo->user->findOne(array('_id' => $userId));
			
			$lobby = array(
				'_id' => new MongoId(),
				'title' => $title,
				'map' => $level,
				'available' => true,
				'player1' => array(
					'userId' => $userId,
					'faction' => $faction,
					'username' => $user['username']
				)
			);
			
			$this->mongo->lobby->insert($lobby);
			
			return true;
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::removeLobbyInfo()
	 */
	public function removeLobbyInfo($id) {
		try {
			$query = array('_id' => new MongoId($id));
			$this->mongo->lobby->remove($query);
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::addPlayerToLobby()
	 */
	public function addPlayerToLobby($lobbyId, $userId, $username, $faction) {
		try {
			$query = array('_id' => new MongoId($lobbyId));
	
			$update = array('$set' => array(
				'player2.userId' => $userId,
				'player2.username' => $username,
				'player2.faction' => $faction
			));
			
			$this->mongo->lobby->update($query, $update);
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::checkAvailableGames()
	 */
	public function checkAvailableGames($page, $level, $faction, $userId) {
		try {
			
			$query = array(
				'map' => $level,
				'player2' => null,
				'player1.userId' => array(
					'$ne' => $userId
				),
				'player1.faction' => array(
					'$ne' => $faction
				)
			);
			
			if($page >= 0) {
				$result = $this->mongo->lobby->find($query)->limit(GAME_LIST_RESULTS_PER_PAGE)->skip(GAME_LIST_RESULTS_PER_PAGE * $page);
			} else {
				$result = $this->mongo->lobby->find($query);
			}
			
			if($result->count() == 0) {
				return false;
			}
			
			return iterator_to_array($result, false);
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::getUserLobbies()
	 */
	public function getUserLobbies($userId, $page) {
		try {
			
			$query = array('$or' => array(
				array('player1.userId' => $userId),
				array('player2.userId' => $userId)
			));
			
			if($page >= 0) {
				$result = $this->mongo->lobby->find($query)->limit(GAME_LIST_RESULTS_PER_PAGE)->skip(GAME_LIST_RESULTS_PER_PAGE * $page);
			} else {
				$result = $this->mongo->lobby->find($query);
			}
			
			if($result->count() == 0) {
				return false;
			}

			return iterator_to_array($result, false);
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::getLobby()
	 */
	public function getLobby($lobbyId) {
		try {
			$query = array('_id' => new MongoId($lobbyId));
			$lobby = $this->mongo->lobby->findOne($query);
			return $lobby;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * @see ILobby::leaveLobby()
	 */
	public function leaveLobby($lobbyId) {
		try {
			$query = array('_id' => new MongoId($lobbyId));
			
			$update = array('$unset' => array(
				'player2' => true
			));
			
			$this->mongo->lobby->update($query, $update);
			
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
}