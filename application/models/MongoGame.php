<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Game model MongoDB implementation
 * @author Escobeitor
 *
 */
class MongoGame extends CI_Model {
	
	/**
	 * Database instance
	 * @var
	 */
	public $mongo;
	
	/**
	 * Default constructor. Load MongoDB library.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->config->load('mongodb');
		$this->mongo = buildMongoConnection();
	}
	
	/**
	 * @see IGame::retrieveGameData()
	 */
	public function retrieveGameData($gameId){
		try {
			
			$query = array('_id' => new MongoId($gameId));
				
			return $this->mongo->game->findOne($query);
				
		} catch(Exception $e) {
			return false;
		}
	}

	/**
	 * @see IGame::retrieveUnfinishedGames()
	 */
	public function retrieveUnfinishedGames($playerId){
		try {
			$query = array('$or' => array(
				array('player1.userId' => $playerId),
				array('player2.userId' => $playerId)
			));
			
			//Select only relevant fields for the player
			$fields = array(
				'player1.username' => true,
				'player1.userId' => true,
				'player2.userId' => true,
				'player2.username' => true,
				'turns' => true,
				'currentTurn' => true,
				'map' => true,
				'title' => true,
				'creationDate' => true,
				'player1.faction' => true
			);
			
			$result = $this->mongo->game->find($query, $fields);
			
			if($result->count() == 0) {
				return array();
			}

			return iterator_to_array($result, false);
			
		} catch(Exception $e) {
			return array();
		}
	}

	/**
	 * @see IGame::updateGame()
	 */
	public function updateGame($gameId, $fields, $values){
		
	}

	/**
	 * @see IGame::delete()
	 */
	public function delete($gameId){
		
	}
	
	/**
	 * @see IGame::save()
	 */
	public function save($gameData) {
		try {
			$gameData['_id'] = new MongoId();
			$this->mongo->game->insert($gameData);
			return $gameData;
		} catch(Exception $e) {
			return false;
		}
	}
	
}