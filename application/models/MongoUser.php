<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User model MongoDB implementation
 * @author Escobeitor
 *
 */
class MongoUser extends CI_Model {
	
	/**
	 * Database instance
	 * @var
	 */
	public $mongo;
	
	/**
	 * Default constructor. Load MongoDB library.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->config->load('mongodb');
		$this->mongo = buildMongoConnection();
	}

	/**
	 * @see IUser::save()
	 */
	public function save($email, $authKey, $registrationId, $userId) {
		try {
			$user = $this->mongo->user->findOne(array(
				'_id' => $userId
			));
			
			if($user) {
				$this->mongo->user->update(
				array(
					"_id" => $userId
				), 
				array(
					'$set' => array(
						"registrationId" => $registrationId,
						"email" => $email,
						"authKey" => $authKey
					)
				));
				
				return $user;
			}
			
			$user = array(
				'_id' => $userId,
				'authKey' => $authKey,
				'email' => $email,
				'registrationId' => $registrationId,
				'lost' => 0,
				'won' => 0,
				'resigned' => 0
			);

			$this->mongo->user->insert($user);
			return $user;
		} catch (Exception $e) {
			return false;
		}
	}

	/**
	 * @see IUser::manageResultCounter()
	 */
	public function manageResultCounter($userId, $amount, $result) {
		
	}

	/**
	 * @see IUser::saveUsername()
	 */
	public function saveUsername($userId, $username) {
		try {
			$criteria = array('_id' => $userId);
			$update = array('$set' => array('username' => $username));
			
			return $this->mongo->user->update($criteria, $update);
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Get user data
	 * @param $userId
	 * @return
	 */
	public function getUser($userId) {
		try {
			$user = $this->mongo->user->findOne(array('_id' => $userId));
			if($user) {
				return $user;
			}
			return false;
		} catch(Exception $e) {
			return false;
		}
	}
}