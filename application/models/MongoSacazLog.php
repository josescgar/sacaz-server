<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Log model MongoDB implementation
 * @author Escobeitor
 *
 */
class MongoSacazLog extends CI_Model {
	
	/**
	 * Database instance
	 * @var
	 */
	public $mongo;
	
	/**
	 * Default constructor. Load MongoDB library.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->config->load('mongodb');
		$this->mongo = buildMongoConnection();
	}
	
	/**
	 * @see ILog::retrieveLog()
	 */
	public function retrieveLog($gameId, $recipientId) {
		try {
			
			$query = array('gameId' => $gameId, 'recipientId' => $recipientId);
			return $this->mongo->log->findOne($query);
			
		} catch(Exception $e) {
			return null;
		}
	}

	/**
	 * @see ILog::save()
	 */
	public function save($log) {
		try {
			$this->mongo->log->insert($log);
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Delete a log file
	 * @param $logId
	 * @param $recipientId
	 * @return boolean
	 */
	public function deleteLog($logId, $recipientId) {
		try {
				
			$query = array('_id' => new MongoId($logId), 'recipientId' => $recipientId);
			$this->mongo->log->remove($query);
			return true;
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Delete pending log files for a user
	 * for the given game
	 * @param $gameId
	 * @param $recipientId
	 * @return boolean
	 */
	public function deleteUserLogs($gameId, $recipientId) {
		try {
	
			$query = array('gameId' => $gameId, 'recipientId' => $recipientId);
			$this->mongo->log->remove($query);
			return true;
				
		} catch(Exception $e) {
			return false;
		}
	}
}