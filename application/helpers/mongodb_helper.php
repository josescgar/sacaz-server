<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function buildMongoConnection() {
		$CI =& get_instance();
		$host = $CI->config->item('mongo_host');
		$db = $CI->config->item('mongo_database');
		$user = $CI->config->item('mongo_username');
		$password = $CI->config->item('mongo_password');
		$m = new MongoClient("mongodb://${user}:${password}@${host}/${db}");
		return $m->$db;
	}