<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * Generates a JSONObject following the standard
	 * response structure with the given data.
	 * @param $status
	 * @param $message
	 * @param $payload
	 */
	function generateResponse($status, $message, $payload) {
		$data = array('status' => $status, 'msg' => $message, 'data' => $payload);
		return json_encode($data);
	}