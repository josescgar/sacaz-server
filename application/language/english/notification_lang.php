<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['notification_lobby_deleted'] = "The user %s has deleted the lobby \"%s\"";
$lang['notification_lobby_user_joined'] = "The user %s joined your lobby \"%s\"";
$lang['notification_lobby_user_left'] = "An opponent left your lobby \"%s\"";
$lang['notification_game_started'] = "The game \"%s\" has started!";
$lang['notification_turn_started'] = "Is your turn in the game \"%s\"!";