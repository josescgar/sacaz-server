<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	
	/**
	 * Load Game model upon intialization
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MongoUser');
	}
	
	/**
	 * Checks if the given authKey (retrieved from POST)
	 * is a valid google play auth key and if there is already
	 * a user associated to it. If the user exists, updates
	 * the device registration ID (POST)
	 */
	public function login() {
		$this->output->set_content_type('application/json');

		$regId = encode_php_tags($this->input->post('regId'));

		if(empty($regId)) {
			$response = generateResponse(StatusCode::RegistrationIdRequired, "No RegId supplied", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!$userData = $this->checkRequestSecurity()) {
				return;
		}
		
		if(!$user = $this->MongoUser->save($userData['email'], $this->input->post('authKey'), $regId, $userData['id'])) {
			$response = generateResponse(StatusCode::KO, "Problem saving user details", null);
			$this->output->set_output($response);
			return;
		}

		$user['authKey']='';
		$user['registrationId']='';
		
		if(!array_key_exists('username', $user)) {
			$response = generateResponse(StatusCode::UsernameRequired, "Username not set", $user);
			$this->output->set_output($response);
			return;
		}
		
		$response = generateResponse(StatusCode::OK, null, $user);
		$this->output->set_output($response);
	}
	
	/**
	 * Saves a new user with the given username and
	 * all date related to the authKey (POST)
	 * @param $username
	 */
	public function save($username) {
		$this->output->set_content_type('application/json');
		
		if(!$userData = $this->checkRequestSecurity()) {
				return;
		}
		
		if(empty($username) || strlen($username) < USERNAME_MIN_LENGTH || strlen($username) > USERNAME_MAX_LENGTH) {
			$response = generateResponse(StatusCode::InvalidUsername, "Username not valid", null);
			$this->output->set_output($response);
			return;
		}
		
		if($this->MongoUser->saveUsername($userData['id'], $username)) {
			$response = generateResponse(StatusCode::OK, null, $username);
		} else {
			$response = generateResponse(StatusCode::KO, "Problem saving username", null);
		}
		
		$this->output->set_output($response);
	}
	
	/**
	 * Perform common controller security operations
	 * @return
	 */
	private function checkRequestSecurity() {
		$apiKey = encode_php_tags($this->input->post('apiKey'));
		$authKey = encode_php_tags($this->input->post('authKey'));
	
		$check = $this->sacazsecurity->checkAPISecurity($apiKey);
		if($check != StatusCode::OK) {
			$response = generateResponse($check, "Error validating keys", null);
			$this->output->set_output($response);
			return false;
		}
	
		if(!$userData = $this->sacazsecurity->validateKey($authKey)) {
			$response = generateResponse(StatusCode::InvalidAuthKey, "Authentication key not valid", null);
			$this->output->set_output($response);
			return false;
		}
	
		return $userData;
	}
}