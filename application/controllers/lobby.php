<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lobby actions controller
 * @author Escobeitor
 *
 */
class Lobby extends CI_Controller { 
	
	/**
	 * Load lobby model upon intialization
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MongoLobby');
	}

	/**
	 * Create a new lobby
	 */
	public function create() {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		$title = encode_php_tags($this->input->post('title'));
		$level = encode_php_tags($this->input->post('level'));
		$faction = encode_php_tags($this->input->post('faction'));
		
		if(!$this->MongoLobby->isTitleAvailable($title)) {
			$response = generateResponse(StatusCode::TitleNotAvailable, "Game title not available", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!$this->MongoLobby->registerLobby($title, $level, $faction, $userData['id'])) {
			$response = generateResponse(StatusCode::KO, "Error creating lobby", null);
			$this->output->set_output($response);
			return;
		}
		
		$response = generateResponse(StatusCode::OK, null, null);
		$this->output->set_output($response);
	}

	/**
	 * Remove the given lobby
	 */
	public function remove($id) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		$lobby = $this->MongoLobby->getLobby($id);
		if($lobby['player1']['userId'] != $userData['id']) {
			$response = generateResponse(StatusCode::KO, "The user can't delete the lobby", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!$this->MongoLobby->removeLobbyInfo($id)) {
			$response = generateResponse(StatusCode::KO, "Error deleting lobby", null);
		} else {
			$response = generateResponse(StatusCode::OK, null, null);
		}
		
		if(!empty($lobby['player2'])) {
			$this->load->library('SacazNotification');
			$this->sacaznotification->notifyPlayerAboutLobbyDeletion($lobby);
		}
		
		$this->output->set_output($response);
	}

	/**
	 * Search and join the lobby identified by the supplied title
	 */
	public function join($lobbyId) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		$username = encode_php_tags($this->input->post('username'));
		$faction = encode_php_tags($this->input->post('faction'));
		
		if(!$this->sacazsecurity->validateUsername($username, $userData['id']) 
				|| !$this->sacazsecurity->validateFaction($faction)) {
			
			$response = generateResponse(StatusCode::KO, "Invalid lobby data", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!$this->MongoLobby->addPlayerToLobby($lobbyId, $userData['id'], $username, $faction)) {
			$response = generateResponse(StatusCode::KO, "Problem joining lobby", null);
			$this->output->set_output($response);
			return;
		}
		
		$this->load->library('SacazNotification');
		$this->sacaznotification->sendUserJoinedNotification($lobbyId);
		
		$response = generateResponse(StatusCode::OK, null, $this->MongoLobby->getLobby($lobbyId));
		$this->output->set_output($response);
	}
	
	/**
	 * Get a list of the user's own lobbies
	 * @param $page
	 */
	public function mine($page) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
				return;
		}
		
		if(!is_numeric($page)) {
			$response = generateResponse(StatusCode::KO, "Wrong page number", null);
			$this->output->set_output($response);
			return;
		}
		
		$lobbies = $this->MongoLobby->getUserLobbies($userData['id'], $page);
		if(!$lobbies) {
			$lobbies=array();
		}
		
		$response = generateResponse(StatusCode::OK, null, $lobbies);
		
		$this->output->set_output($response);
	}

	/**
	 * List all lobies matching the criteria
	 */
	public function listlobbies($page) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(!is_numeric($page)) {
			$response = generateResponse(StatusCode::KO, "Wrong page number", null);
			$this->output->set_output($response);
			return;
		}
		
		$level = encode_php_tags($this->input->post('level'));
		$faction = encode_php_tags($this->input->post('faction'));
		
		$lobbies = $this->MongoLobby->checkAvailableGames($page, $level, $faction, $userData['id']);
		if(!$lobbies) {
			$lobbies = array();
		}
		
		$this->output->set_output(generateResponse(StatusCode::OK, null, $lobbies));
	}
	
	/**
	 * Remove player from lobby
	 * @param $id
	 */
	public function leave($id) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(!$this->MongoLobby->leaveLobby($id)) {
			$response = generateResponse(StatusCode::KO, "Can't leave lobby", null);
			$this->output->set_output($response);
			return;
		}
		
		$this->load->library('SacazNotification');
		$this->sacaznotification->sendUserLeftNotification($id);
		
		$this->output->set_output(generateResponse(StatusCode::OK, null, null));
	}
	
	/**
	 * Get lobby information
	 * @param $id
	 */
	public function get($id) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(!$lobby = $this->MongoLobby->getLobby($id)) {
			$response = generateResponse(StatusCode::KO, "Can't get lobby", null);
		} else {
			$response = generateResponse(StatusCode::OK, null, $lobby);
		}
		
		$this->output->set_output($response);
	}
	
	/**
	 * Perform common controller security operations
	 * @return
	 */
	private function checkRequestSecurity() {
		$apiKey = encode_php_tags($this->input->post('apiKey'));
		$authKey = encode_php_tags($this->input->post('authKey'));
	
		$check = $this->sacazsecurity->checkAPISecurity($apiKey);
		if($check != StatusCode::OK) {
			$response = generateResponse($check, "Error validating keys", null);
			$this->output->set_output($response);
			return false;
		}
	
		if(!$userData = $this->sacazsecurity->validateKey($authKey)) {
			$response = generateResponse(StatusCode::InvalidAuthKey, "Authentication key not valid", null);
			$this->output->set_output($response);
			return false;
		}
	
		return $userData;
	}
}