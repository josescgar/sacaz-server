<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Game extends CI_Controller {
	
	/**
	 * Load Game model upon intialization
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MongoGame');
		$this->load->model('MongoSacazLog');
	}
	
	/**
	 * Retrieves a game
	 * @param $gameId
	 */
	public function retrieve($gameId) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(!$game = $this->MongoGame->retrieveGameData($gameId)) {
			$response = generateResponse(StatusCode::KO, "No game found", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!($game['player1']['userId'] == $userData['id'] || $game['player2']['userId'] == $userData['id'])) {
			$response = generateResponse(StatusCode::KO, "The user can't retrieve this game", null);
			$this->output->set_output($response);
			return;
		}
		
		//Remove pending logs for the user who requested the game file
		$this->MongoSacazLog->deleteUserLogs($gameId, $userData['id']);
		
		$opponentUser = $game['player1']['userId'] == $userData['id'] ? "player2" : "player1";
		
		//Remove sensible information from the opponent
		$game[$opponentUser]['actionPoints'] = '';
		$game[$opponentUser]['resources'] = '';
		$game[$opponentUser]['energyDistribution'] = '';
		$game[$opponentUser]['items'] = '';
		$game[$opponentUser]['ranger']['allocation'] = '';
		
		$this->output->set_output(generateResponse(StatusCode::OK, null, $game));
	}
	
	/**
	 * Retrieves a list of unfinished games for the given player
	 */
	public function unfinished() {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		$games = $this->MongoGame->retrieveUnfinishedGames($userData['id']);
		
		$this->output->set_output(generateResponse(StatusCode::OK, null, $games));
	}
	
	/**
	 * Retrieves the given log data
	 * @param $gameId
	 */
	public function log($gameId) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(empty($gameId) || !is_string($gameId)) {
			$response = generateResponse(StatusCode::KO, "Wrong game ID", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!$log = $this->MongoSacazLog->retrieveLog($gameId, $userData['id'])){
			$response = generateResponse(StatusCode::KO, "No log found", null);
		} else {
			$response = generateResponse(StatusCode::OK, null, $log);
		}

		$this->output->set_output($response);
	}
	
	/**
	 * Deletes a log after the user confirms it has been read
	 * @param $logId
	 */
	public function logRead($logId) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(empty($logId) || !is_string($logId)) {
			$response = generateResponse(StatusCode::KO, "Wrong log ID", null);
			$this->output->set_output($response);
			return;
		}
		
		$this->MongoSacazLog->deleteLog($logId, $userData['id']);
		$response = generateResponse(StatusCode::OK, null, null);
		$this->output->set_output($response);
	}
	
	/**
	 * Processes a new log (LOG received by POST)
	 */
	public function processLog($gameId) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		if(!$game = $this->MongoGame->retrieveGameData($gameId)) {
			$response = generateResponse(StatusCode::KO, "Wrong game ID", null);
			$this->output->set_output($response);
			return;
		}
		
		if(!$logData = json_decode(encode_php_tags($this->input->post('logData')), true, 512)) {
			$response = generateResponse(StatusCode::KO, "Problem decoding log data", null);
			$this->output->set_output($response);
			return;
		}
		
		$logData['gameId'] = $gameId;
		$logData['creatorId'] = $userData['id'];
		$playerKey = $game['player1']['userId'] == $userData['id'] ? 'player1' : 'player2';
		$logData['recipientId'] = $game['player1']['userId'] == $userData['id'] ? $game['player2']['userId'] : $game['player1']['userId'];
		$logData['date'] = time() * 1000;
		
		$this->load->library('SacazLog');
		if(!$this->sacazlog->process($logData, $playerKey)) {
			$response = generateResponse(StatusCode::KO, "Error processing the log", null);
			$this->output->set_output($response);
			return;
		}
		
		$this->MongoSacazLog->save($logData);
		
		$this->load->library('SacazNotification');
		$this->sacaznotification->sendTurnNotification($game, $logData['recipientId']);
		
		$response = generateResponse(StatusCode::OK, null, null);
		$this->output->set_output($response);
	}
	
	/**
	 * Registers a game result
	 * @param $userId
	 * @param $result
	 */
	public function finish($userId, $result) {
		
	}
	
	/**
	 * Creates a new game associated with the 
	 * given lobby
	 * @param $lobbyId
	 */
	public function create($lobbyId) {
		$this->output->set_content_type('application/json');
		if(!$userData = $this->checkRequestSecurity()) {
			return;
		}
		
		$this->load->model('MongoLobby');
		if(!$lobby = $this->MongoLobby->getLobby($lobbyId)) {
			$response = generateResponse(StatusCode::KO, "No lobby found when creating game", null);
			$this->output->set_output($response);
			return;
		}
		
		if($lobby['player1']['userId'] != $userData['id']) {
			$response = generateResponse(StatusCode::KO, "The user can't start the game", null);
			$this->output->set_output($response);
			return;
		}
		
		if(empty($lobby['player1']) || empty($lobby['player2'])) {
			$response = generateResponse(StatusCode::KO, "The game don't have enough players", null);
			$this->output->set_output($response);
			return;
		}
		
		$baseGame = json_decode($this->load->file('application/assets/baseGame.json', true), true);
		if(empty($baseGame)) {
			$response = generateResponse(StatusCode::KO, "Error loading base game", null);
			$this->output->set_output($response);
			return;
		}
		
		//Fill the rest of the game data
		$baseGame['currentTurn'] = $lobby['player1']['userId'];
		$baseGame['creationDate'] = time() * 1000;
		$baseGame['lastTurnDate'] = time() * 1000;
		$baseGame['map'] = $lobby['map'];
		$baseGame['title'] = $lobby['title'];
		
		$baseGame['player1']['userId'] = $lobby['player1']['userId'];
		$baseGame['player1']['username'] = $lobby['player1']['username'];
		$baseGame['player1']['faction'] = $lobby['player1']['faction'];
		
		$baseGame['player2']['userId'] = $lobby['player2']['userId'];
		$baseGame['player2']['username'] = $lobby['player2']['username'];
		$baseGame['player2']['faction'] = $lobby['player2']['faction'];
		
		//Save the game
		if(!$baseGame = $this->MongoGame->save($baseGame)) {
			$response = generateResponse(StatusCode::KO, "Error saving game data", null);
			$this->output->set_output($response);
			return;
		}
		
		//Delete the lobby
		$this->MongoLobby->removeLobbyInfo($lobbyId);
		
		//Notify the opponent
		$this->load->library('SacazNotification');
		$this->sacaznotification->sendGameStartNotification((string) $baseGame['_id'], $baseGame['player2']['userId'], $baseGame['title']);
		
		//Send the game to the player
		$this->output->set_output(generateResponse(StatusCode::OK, null, $baseGame));
	}
	
	/**
	 * Perform common controller security operations
	 * @return
	 */
	private function checkRequestSecurity() {
		$apiKey = encode_php_tags($this->input->post('apiKey'));
		$authKey = encode_php_tags($this->input->post('authKey'));
	
		$check = $this->sacazsecurity->checkAPISecurity($apiKey);
		if($check != StatusCode::OK) {
			$response = generateResponse($check, "Error validating keys", null);
			$this->output->set_output($response);
			return false;
		}
	
		if(!$userData = $this->sacazsecurity->validateKey($authKey)) {
			$response = generateResponse(StatusCode::InvalidAuthKey, "Authentication key not valid", null);
			$this->output->set_output($response);
			return false;
		}
	
		return $userData;
	}
}