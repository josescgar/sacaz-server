<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sacaz logging library
 * @author Escobeitor
 *
 */
class SacazLog {
	
	/**
	 * Codeigniter instance
	 */
	private $CI;
	
	/**
	 * Database instance
	 */
	private $mongo;
	
	/**
	 * Current gameId
	 */
	private $gameId;
	
	/**
	 * Current user player key
	 */
	private $playerKey;
	
	/**
	 * Load codeigniter used libraries and files
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->config->load('mongodb');
		$this->mongo = buildMongoConnection();
	}
	
	/**
	 * Processes the given log
	 * @param $log
	 */
	public function process($log, $playerKey) {
		$this->gameId = $log['gameId'];
		$this->playerKey = $playerKey;
		
		for($i = 0; $i < count($log['actions']); $i++) {
			$action = $log['actions'][$i];
			switch($action['type']) {
				case ActionType::NewCharacter:
					if(!$this->newCharacter($action)) {
						return false;
					}
					break;
				case ActionType::CharacterMovement:
					if(!$this->characterMovement($action)) {
						return false;
					}
					break;
				case ActionType::NewNPC:
					if(!$this->newNPC($action)) {
						return false;
					}
					break;
				case ActionType::NPCMovement:
					if(!$this->NPCMovement($action)) {
						return false;
					}
					break;
				case ActionType::TurnFinished:
					if(!$this->turnFinished($action, $log['date'])) {
						return false;
					}
					break;
				case ActionType::NPCStrike:
					if(!$this->NPCStrike($action)) {
						return false;
					}
					break;
				case ActionType::Strike:
					if(!$this->Strike($action)) {
						return false;
					}
					break;
				default:
					break;
			}
		}
		
		return true;
	}
	
	/**
	 * Registers a new "Energy distribution" action
	 * @param $action
	 */
	private function energyDistribution($action) {
		
	}
	
	/**
	 * Registers a new "Movement" action
	 * performed by a character
	 * @param $action
	 */
	private function characterMovement($action) {
		try {
			$value = json_decode($action['value'], true);
			
			$query = array(
					'_id' => new MongoId($this->gameId),
					$this->playerKey.'.characters.id' => $action['source']
			);
				
			$update = array(
					'$set' => array(
							$this->playerKey.'.characters.$.position.x' => $value['x'],
							$this->playerKey.'.characters.$.position.y' => $value['y']
					)
			);
				
			$this->mongo->game->update($query, $update);
				
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Registers a new "Movement" action
	 * performed by a NPC
	 * @param $action
	 */
	private function NPCMovement($action) {
		try {
			$value = json_decode($action['value'], true);
			
			$query = array(
					'_id' => new MongoId($this->gameId),
					'npcs.id' => $action['source']
			);
		
			$update = array(
					'$set' => array(
							'npcs.$.position.x' => $value['x'],
							'npcs.$.position.y' => $value['y'],
							'npcs.$.target' => $action['cost']
					)
			);
		
			$this->mongo->game->update($query, $update);
		
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Registers a new "Pick up object" action
	 * @param $action
	 */
	private function pickUpObject($action) {
		
	}
	
	/**
	 * Registers a new "Game finished" action
	 * @param $action
	 */
	private function gameFinished($action) {
		
	}
	
	/**
	 * Registers a new "Ability used" action
	 * @param $action
	 */
	private function abilityUsed($action) {
		
	}
	
	/**
	 * Registers a new "Ranger raffle" action
	 * @param $action
	 */
	private function rangerRaffle($action) {
		
	}
	
	/**
	 * Registers a new "Turn finished" action
	 * @param $action
	 */
	private function turnFinished($action, $turnDate) {
		try {
			$cost = json_decode($action['cost'], true);
			$value = json_decode($action['value'], true);
			
			$query = array('_id' => new MongoId($this->gameId));
			
			$update = array(
					'$set' => array(
							$this->playerKey.'.actionPoints' => $cost['ap'],
							$this->playerKey.'.resources.food' => $cost['food'],
							$this->playerKey.'.resources.materials' => $cost['materials'],
							$this->playerKey.'.resources.water' => $cost['water'],
							$this->playerKey.'.resources.electricity' => $cost['electricity'],
							$this->playerKey.'.resources.electricity' => $cost['electricity'],
							'lastTurnDate' => $turnDate,
							'currentTurn' => $value['currentTurn'],
							'turns' => $value['turns'],
							'numNPC' => $value['numNPC']
					)
			);
		
			$this->mongo->game->update($query, $update);
		
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Registers a new "Ranger switch" action
	 * @param $action
	 */
	private function rangerSwitch($action) {
		
	}
	
	/**
	 * Registers a new "Strike" action
	 * @param $action
	 */
	private function strike($action) {
		try {
			
			//UPDATE HEALTH
			$value = json_decode($action['value'], true);
			
			$query = array(
					'_id' => new MongoId($this->gameId),
					$value['targetKey'].'.id' => $value['targetId']
			);
			
			$update = array(
				'$set' => array(
					$value['targetKey'].'.$.health' => $value['finalHealth']
				)
			);
			
			$this->mongo->game->update($query, $update);
			
			$query = array($value['targetKey'].'health' => array('$lte' => 0));
			$this->mongo->lobby->remove($query);
			
			//UPDATE AMMO
			$query = array(
					'_id' => new MongoId($this->gameId),
					$this->playerKey.'.characters.weapons.id' => $value['weaponId']
			);
				
			$update = array(
					'$inc' => array(
							$this->playerKey.'.characters.$.weapons.'.$value['weaponIndex'].'.ammo' => -1
					)
			);
			
			$this->mongo->game->update($query, $update);
			return true;
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Registers a new "Ranger resources" action
	 * @param $action
	 */
	private function rangerResources($action) {
		
	}
	
	/**
	 * Registers a new "New character" action
	 * @param $action
	 */
	private function newCharacter($action) {
		try {
			$value = json_decode($action['value'], true);
			
			$query = array('_id' => new MongoId($this->gameId));
			
			$update = array(	
				'$push' => array(
					$this->playerKey.'.characters' => $value
				)
			);
			
			$this->mongo->game->update($query, $update);
			
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Registers a new NPC
	 * @param $action
	 */
	private function newNPC($action) {
		try {
			$value = json_decode($action['value'], true);
			
			$query = array('_id' => new MongoId($this->gameId));
				
			$update = array(
					'$push' => array(
							'npcs' => $value
					)
			);
				
			$this->mongo->game->update($query, $update);
				
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Registers a new NPC strike
	 * @param $action
	 */
	private function NPCStrike($action) {
		try {
			
			$query = array(
					'_id' => new MongoId($this->gameId),
					$action['cost'].'.characters.id' => $action['source']
			);
				
			$update = array(
					'$set' => array(
							$action['cost'].'.characters.$.health' => (int) $action['value']
					)
			);
			
			$this->mongo->game->update($query, $update, array("w" => 0));
			
			$query = array($action['cost'].'.characters.health' => array('$lte' => 0));
			$this->mongo->lobby->remove($query);
	
			return true;
		} catch(Exception $e) {
			return false;
		}
	}
}