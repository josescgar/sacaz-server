<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Possible log actions
 * @author Escobeitor
 *
 */
class ActionType {
	
	const NewCharacter = "NEW_CHARACTER";
	
	const NewNPC = "NEW_NPC";
	
	const EnergyDistribution = "ENERGY_DISTRIBUTION";
	
	const RangerResources = "RANGER_RESOURCES";
	
	const CharacterMovement = "CHARACTER_MOVEMENT";
	
	const NPCMovement = "NPC_MOVEMENT";
	
	const PickUpObject = "PICK_UP_OBJECT";
	
	const Strike = "STRIKE";
	
	const RangerSwitch = "RANGER_SWITCH";
	
	const GameFinished = "GAME_FINISHED";
	
	const TurnFinished = "TURN_FINISHED";
	
	const RangerRaffle = "RANGER_RAFFLE";
	
	const AbilityUsed = "ABILITY_USED";
	
	const NPCStrike = "NPC_STRIKE";
}
