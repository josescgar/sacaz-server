<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sacaz security related functions
 * @author Escobeitor
 *
 */
class SacazSecurity {
	
	/**
	 * Valid factions in the game
	 */
	private $factions = array('SURVIVORS', 'CRIMINALS');
	
	/**
	 * Validates the given Google Play Service
	 * authentication key
	 * @param $authKey
	 */
	public function validateKey($authKey) {
		try {
			$CI =& get_instance();
			$CI->load->library('google/Google_Client');
			$CI->load->library('google/auth/Google_Oauth2');

			$ver = $CI->google_oauth2->verifyIdToken($authKey);
			
			return $ver->getAttributes()['payload'];
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Checks the validity of both the api key and
	 * the google authentication key
	 * @param $apiKey
	 * @param $authKey
	 * @return an OK or error code
	 */
	public function checkAPISecurity($apiKey) {
		if(empty($apiKey) || $apiKey != GAME_API_KEY) {
			return StatusCode::InvalidAPIKey;
		}
	
		return StatusCode::OK;
	}
	
	/**
	 * Check if the given faction is a valid game faction
	 * @param $faction
	 * @return boolean
	 */
	public function validateFaction($faction) {
		return in_array($faction, $this->factions);
	}
	
	/**
	 * Check if the given username matches the real one
	 * @param $username
	 * @param $userId
	 */
	public function validateUsername($username, $userId) {
		$CI =& get_instance();
		$CI->config->load('mongodb');
		$mongo = buildMongoConnection();
		
		try {
			$query = array('_id' => $userId, 'username' => $username);
			return $mongo->user->count($query) > 0;
		} catch(Exception $e) {
			return false;
		}
	}
}