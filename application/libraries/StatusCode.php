<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Possible response statuses
 * @author Escobeitor
 *
 */
class StatusCode {
	
	const OK = 1;
	
	const KO = 2;
	
	const UsernameRequired = 3;
	
	const InvalidAuthKey = 4;
	
	const InvalidAPIKey = 5;
	
	const RegistrationIdRequired = 6;
	
	const InvalidUsername = 7;
	
	const TitleNotAvailable = 8;
}
