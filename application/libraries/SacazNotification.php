<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * GCM Notification wrapper
 * @author Escobeitor
 *
 */
class SacazNotification {
	
	/**
	 * Codeigniter instance
	 */
	var $CI;
	
	/**
	 * Load codeigniter used libraries and files
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('gcm');
		$this->CI->load->model('MongoUser');
		$this->CI->lang->load('notification', 'english');
	}
	
	/**
	 * Notifies a Player (the one who did not created the lobby)
	 * about a lobby deletion
	 * @param $lobbyId
	 */
	public function notifyPlayerAboutLobbyDeletion($lobby) {
		$opponent = $this->CI->MongoUser->getUser($lobby['player2']['userId']);
		
		$message = sprintf($this->CI->lang->line('notification_lobby_deleted'), $lobby['player1']['username'], $lobby['title']);
		$regId = $opponent['registrationId'];
		
		try {
			
			$this->CI->gcm->setMessage($message);
			$this->CI->gcm->addRecepient($regId);
			$this->CI->gcm->setGroup((string) $lobby['_id']);
			$this->CI->gcm->setData(array(
					'lobbyId' => (string) $lobby['_id'],
					'type' => 'LOBBY',
					'action' => 'DELETE'
			));
			
			if($this->CI->gcm->send()) {
				return true;
			} else {
				return false;
			}
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Notifies the next turn owner that 
	 * he can starts the new turn
	 * @param $gameId
	 */
	public function sendTurnNotification($game, $recipient) {
		
		if(!$game) {
			return false;
		}
		
		$opponent = $this->CI->MongoUser->getUser($recipient);
		
		$message = sprintf($this->CI->lang->line('notification_turn_started'), $game['title']);
		$regId = $opponent['registrationId']; 
		
		try {
				
			$this->CI->gcm->setMessage($message);
			$this->CI->gcm->addRecepient($regId);
			$this->CI->gcm->setGroup((string) $game['_id']);
			$this->CI->gcm->setData(array(
					'gameId' => (string) $game['_id'],
					'type' => 'GAME',
					'action' => 'TURN_START'
			));
				
			if($this->CI->gcm->send()) {
				return true;
			} else {
				return false;
			}
				
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Notifies the given player that a new game
	 * has started
	 * @param $playerId
	 */
	public function sendGameStartNotification($gameId, $playerId, $gameTitle) {
		$opponent = $this->CI->MongoUser->getUser($playerId);

		$message = sprintf($this->CI->lang->line('notification_game_started'), $gameTitle);
		$regId = $opponent['registrationId'];
		
		try {
				
			$this->CI->gcm->setMessage($message);
			$this->CI->gcm->addRecepient($regId);
			$this->CI->gcm->setGroup($gameId);
			$this->CI->gcm->setData(array(
					'gameId' => $gameId,
					'type' => 'GAME',
					'action' => 'START'
			));
				
			if($this->CI->gcm->send()) {
				return true;
			} else {
				return false;
			}
				
		} catch(Exception $e) {
			return false;
		}
		
	}
	
	/**
	 * Notifies the given player that a game has ended
	 * @param $playerId
	 */
	public function sendGameEndedNotification($gameId, $playerId) {
		
	}
	
	/**
	 * Notify the creator of the lobby that a user has 
	 * joined his lobby
	 * @param $lobbyId
	 * @param $username
	 */
	public function sendUserJoinedNotification($lobbyId) {
		$this->CI->load->model('MongoLobby');
		$lobby = $this->CI->MongoLobby->getLobby($lobbyId);
		$creator = $this->CI->MongoUser->getUser($lobby['player1']['userId']);
		
		$message = sprintf($this->CI->lang->line('notification_lobby_user_joined'), $lobby['player2']['username'], $lobby['title']);
		
		try {
			
			$this->CI->gcm->setMessage($message);
			$this->CI->gcm->addRecepient($creator['registrationId']);
			$this->CI->gcm->setGroup($lobbyId);
			$this->CI->gcm->setData(array(
					'lobbyId' => $lobbyId,
					'type' => 'LOBBY',
					'action' => 'JOIN'
			));
			
			if($this->CI->gcm->send()) {
				return true;
			} else {
				return false;
			}
			
		} catch(Exception $e) {
			return false;
		}
	}
	
	/**
	 * Notify the creator of the lobby that another
	 * user left his lobby
	 * @param $lobbyId
	 * @param $username
	 */
	public function sendUserLeftNotification($lobbyId) {
		$this->CI->load->model('MongoLobby');
		$lobby = $this->CI->MongoLobby->getLobby($lobbyId);
		$creator = $this->CI->MongoUser->getUser($lobby['player1']['userId']);
	
		$message = sprintf($this->CI->lang->line('notification_lobby_user_left'), $lobby['title']);
	
		try {
				
			$this->CI->gcm->setMessage($message);
			$this->CI->gcm->addRecepient($creator['registrationId']);
			$this->CI->gcm->setGroup($lobbyId);
			$this->CI->gcm->setData(array(
					'lobbyId' => $lobbyId,
					'type' => 'LOBBY',
					'action' => 'LEAVE'
			));
				
			if($this->CI->gcm->send()) {
				return true;
			} else {
				return false;
			}
				
		} catch(Exception $e) {
			return false;
		}
	}
}